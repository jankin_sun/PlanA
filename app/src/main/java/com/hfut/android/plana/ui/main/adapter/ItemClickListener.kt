package com.hfut.android.plana.ui.main.adapter

interface ItemClickListener {
    fun onItemClick(position: Int)
}