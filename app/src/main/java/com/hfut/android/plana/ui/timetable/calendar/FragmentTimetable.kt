package com.hfut.android.plana.ui.timetable.calendar


import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haibin.calendarview.Calendar
import com.haibin.calendarview.CalendarView
import com.hfut.android.plana.DAILY
import com.hfut.android.plana.R
import com.hfut.android.plana.ui.timetable.calendar.adapter.PlanAdapter
import kotlinx.android.synthetic.main.fragment_fragment_timetable.*
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentTimetable : Fragment() {

    private lateinit var timetableViewModel: TimetableViewModel
    private var mYear = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_timetable, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        timetableViewModel = ViewModelProviders.of(this).get(TimetableViewModel::class.java)

            val date = Date()
            val planAdapter = PlanAdapter(
                context!!,
                R.layout.item_view,
                timetableViewModel.getDataByDate(date)
            )
            recyclerView.adapter = planAdapter
            recyclerView.layoutManager = LinearLayoutManager(context)


        /*ib_calendar.setOnClickListener {
            Log.e("MyLog", "-----------------------添加事项--------------------------------")
            val calendarStart = Calendar()
            val calendarEnd = Calendar()
            calendarStart.year = 2019
            calendarStart.month = 7
            calendarStart.day = 6
            calendarEnd.year = 2019
            calendarEnd.month = 7
            calendarEnd.day = 10
            timetableViewModel.insertPlan("plan1",
                "默认",
                R.mipmap.ic_calendar,
                calendarStart,
                calendarEnd,
                DAILY,
                1
                )
            Log.e("MyLog", "-----------------------添加事项-----------${timetableViewModel.getAllData()}---------------------")
            Log.e("MyLog", "-----------------------添加事项-----------${calendarStart}---------------------")
            Log.e("MyLog", "-----------------------添加事项-----------${calendarEnd}---------------------")
            Log.e("MyLog", "-----------------------添加事项-----------${calendarStart.timeInMillis}---------------------")
            Log.e("MyLog", "-----------------------添加事项-----------${calendarEnd.timeInMillis}---------------------")
        }*/


        calendarView.setOnCalendarSelectListener(object: CalendarView.OnCalendarSelectListener {
            @SuppressLint("SetTextI18n")
            override fun onCalendarSelect(calendar: Calendar?, isClick: Boolean) {
                tv_lunar.visibility = View.VISIBLE
                tv_year.visibility = View.VISIBLE
                tv_month_day.text = calendar?.month!!.toString() + "月" + calendar?.day!! + "日"
                tv_year.text = calendar.year.toString()
                tv_lunar.text = calendar.lunar
                mYear = calendar.year

                Log.e("MyLog", "-----------------------${calendar}--------------------------------")
                Log.e("MyLog", "-----------------------${calendar.timeInMillis}--------------------------------")

                val planAdapter = PlanAdapter(
                    context!!,
                    R.layout.item_view,
                    timetableViewModel.getData(calendar)
                )

                recyclerView.adapter = planAdapter
                recyclerView.layoutManager = LinearLayoutManager(context)
            }

            override fun onCalendarOutOfRange(calendar: Calendar?) {

            }
        })
    }

}
