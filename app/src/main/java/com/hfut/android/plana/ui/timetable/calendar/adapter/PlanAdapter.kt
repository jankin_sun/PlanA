package com.hfut.android.plana.ui.timetable.calendar.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hfut.android.plana.R
import com.hfut.android.plana.database.entity.Plan
import kotlinx.android.synthetic.main.item_view.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.log

class PlanAdapter (
    private val context: Context,
    private val itemLayout: Int,
    private val data: MutableList<Plan>
    ): RecyclerView.Adapter<PlanAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(context).inflate(itemLayout, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val plan = data[position]
        holder.planIcon.setImageResource(plan.portraitId)
        holder.planName.text = plan.planName
        holder.planContent.text = "开始时间： ${SimpleDateFormat("yyyy-MM-dd").format(Date(plan?.startTime!!))} \n" +
                "结束时间： ${SimpleDateFormat("yyyy-MM-dd").format(Date(plan?.endTime!!))}"
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val planIcon: ImageView = itemView.planIcon!!
        val planName: TextView = itemView.planName
        val planContent: TextView = itemView.planContent
    }
}