package com.hfut.android.plana.ui.main.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hfut.android.plana.database.entity.Plan
import android.widget.ImageView
import android.widget.TextView
import com.hfut.android.plana.R
import kotlinx.android.synthetic.main.main_item_view.view.*


class MainAdapter (
    val context: Context,
    val itemLayout: Int,
    val data: MutableList<Plan>,
    val itemClickListener: ItemClickListener
): RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(context).inflate(itemLayout, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mainPlan = data[position]
        holder.mainPlanIcon.setImageResource(mainPlan.portraitId)
        holder.mainPlanName.text = mainPlan.planName
        holder.itemView.setOnClickListener {
            itemClickListener.onItemClick(position)
            holder.mainPlanIcon.setImageResource(R.drawable.icon_completed)
            holder.itemView.isClickable = false
        }
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val mainPlanIcon: ImageView = itemView.main_plan_icon
        val mainPlanName: TextView = itemView.main_plan_name
    }
}