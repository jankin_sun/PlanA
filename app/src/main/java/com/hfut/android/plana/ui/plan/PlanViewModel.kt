package com.hfut.android.plana.ui.plan

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.haibin.calendarview.Calendar
import com.hfut.android.plana.database.entity.Plan
import com.hfut.android.plana.repository.Reposition

class PlanViewModel: ViewModel() {
    val dataCompletePlan = MutableLiveData<Plan>()
    val dataUnCompletePlan = MutableLiveData<Plan>()
    private val repository = Reposition()
    val completePlan:MutableList<Plan> = arrayListOf()
    val unCompletePlan:MutableList<Plan> = arrayListOf()

    fun getTodayPlan(date: Calendar) {
        completePlan.clear()
        unCompletePlan.clear()
        for (i in repository.getTodayCompletePlan(date).indices){
            dataCompletePlan.postValue(repository.getTodayCompletePlan(date).get(i))
            completePlan.add(repository.getTodayCompletePlan(date).get(i))
        }
        for (i in repository.getTodayUncompletePlan(date).indices){
            dataUnCompletePlan.postValue(repository.getTodayUncompletePlan(date).get(i))
            unCompletePlan.add(repository.getTodayUncompletePlan(date).get(i))
        }
    }
}