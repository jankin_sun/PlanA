package com.hfut.android.plana.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.hfut.android.plana.database.entity.Plan

/**
 * Author by yt and ob, Date on 7/5/2019.
 * PS: Consumed 3 bottles of fat-otaku-happy-water to write the code.
 */
@Dao
interface PlanDao: BaseDao<Plan> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(element: Plan)

    @Query("Select * from PlanDatabase")
    fun getAllPlans(): MutableList<Plan>

    @Query("Select * from PlanDatabase where id = :ID")
    fun getPlan(ID:Int): Plan

    @Query("Select * from PlanDatabase order by id desc ")
    fun getAllByDateDesc(): MutableList<Plan>

    @Query("Select * from PlanDatabase where INCOMPLETE_NUM > 0 and :date between START_TIME and END_TIME")
    fun getDatePlan(date: Long): MutableList<Plan>

    @Query("Select * from PlanDatabase where INCOMPLETE_NUM > 0 and IS_COMPLETE = 1 and:date between START_TIME and END_TIME")
    fun getTodayCompletePlan(date:Long): MutableList<Plan>

    @Query("Select * from PlanDatabase where INCOMPLETE_NUM > 0 and IS_COMPLETE = 0 and :date between START_TIME and END_TIME")
    fun getTodayUncompletePlan(date:Long): MutableList<Plan>

    @Query("delete from PlanDatabase")
    fun deleteAll()

    @Query("delete from PlanDatabase where id = :id")
    fun deletePlan(id:Int)

    @Query("update PlanDatabase set IS_COMPLETE = 1, COMPLETE_NUM = COMPLETE_NUM + 1, INCOMPLETE_NUM = INCOMPLETE_NUM - 1 where id = :id")
    fun completePlan(id:Int)

    @Query("update PlanDatabase set IS_COMPLETE = 0 where FREQUENCY = :daily and INCOMPLETE_NUM > 0")
    fun refreshDailyPlan(daily: Int)

    @Query("update PlanDatabase set IS_COMPLETE = 0 where FREQUENCY = :weekly and INCOMPLETE_NUM >0")
    fun refreshWeeklyPlan(weekly: Int)

    @Query("update PlanDatabase set PORTRAIT_ID = :iconId where id = :id")
    fun updateIconById(id:Int,iconId:Int)

}
