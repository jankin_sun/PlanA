package com.hfut.android.plana.util

import androidx.room.TypeConverter
import java.text.DateFormat.getDateInstance
import java.text.SimpleDateFormat
import java.util.*

/**
 * Author by yt and ob, Date on 7/8/2019.
 * PS: Consumed 3 bottles of fat-otaku-happy-water to write the code.
 */

object Converters {

    @TypeConverter
    fun longToDate(value: Long?): String? {
        val sdf = SimpleDateFormat("YYYY-MM-dd", Locale.CHINA)
        val date = Date(value!!)
        return sdf.format(date)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return  date?.time!!.toLong()
    }
}