package com.hfut.android.plana.database.dao

import androidx.room.*
/**
 * Author by yt and ob, Date on 7/5/2019.
 * PS: Consumed 3 bottles of fat-otaku-happy-water to write the code.
 */
@Dao
interface BaseDao<T>{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(list: MutableList<T>)

    @Delete
    fun delete(element: T)

    @Delete
    fun deleteList(elements: MutableList<T>)

    @Delete
    fun deleteSome(vararg elements: T)

    @Update
    fun update(element: T)
}