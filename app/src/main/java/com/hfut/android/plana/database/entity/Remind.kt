package com.hfut.android.plana.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "RemindDatabase")
data class Remind (
    @ColumnInfo(name = "REMIND_NAME")
    var name:String?,
    @ColumnInfo(name = "REMIND_DATE")
    var date: Long?,
    @PrimaryKey(autoGenerate = true)
    var id:Int
)
