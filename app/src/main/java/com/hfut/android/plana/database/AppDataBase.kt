package com.hfut.android.plana.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.haibin.calendarview.Calendar
import com.hfut.android.plana.database.dao.PlanDao
import com.hfut.android.plana.database.dao.RemindDao
import com.hfut.android.plana.database.entity.Plan
import com.hfut.android.plana.database.entity.Remind

/**
 * Author by yt and ob, Date on 7/5/2019.
 * PS: Consumed 3 bottles of fat-otaku-happy-water to write the code.
 */

@Database(entities = [Plan::class,Remind::class],version = 2)
abstract class AppDataBase :RoomDatabase(){
    abstract fun getPlanDao():PlanDao

    abstract fun getRemindDao():RemindDao

    //以单例模式创建数据库
    companion object{
        val instance = Singleton.single
    }

    private object Singleton{
        val single:AppDataBase = Room.databaseBuilder(
            DatabaseApplication.instance(),
            AppDataBase::class.java,
            "PlanA.db"
        )
            .allowMainThreadQueries()
            .build()

    }
}