package com.hfut.android.plana.database


import android.app.Application
import kotlin.properties.Delegates
/**
 * Author by yt and ob, Date on 7/5/2019.
 * PS: Consumed 3 bottles of fat-otaku-happy-water to write the code.
 */
class DatabaseApplication: Application() {

    companion object {
        var instance: DatabaseApplication by Delegates.notNull()
        fun instance() = instance
    }

    override fun onCreate(){
        super.onCreate()

        instance = this
    }
}