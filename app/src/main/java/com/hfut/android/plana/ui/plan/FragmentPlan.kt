package com.hfut.android.plana


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.haibin.calendarview.Calendar
import com.hfut.android.plana.database.entity.Plan
import com.hfut.android.plana.ui.plan.Adapter.PlanAdapter
import com.hfut.android.plana.ui.plan.PlanViewModel
import kotlinx.android.synthetic.main.fragment_fragment_plan.*
import java.text.SimpleDateFormat
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentPlan : Fragment() {
    private lateinit var viewModel: PlanViewModel
    private lateinit var cPlanadapter: PlanAdapter
    private lateinit var uPlanadapter: PlanAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_plan, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_plan_back_main.setOnClickListener {
            findNavController().navigate(R.id.actionBackMain)
        }

        val current = Calendar()
        current.year = SimpleDateFormat("yyyy").format(Date()).toInt()
        current.month = SimpleDateFormat("MM").format(Date()).toInt()
        current.day = SimpleDateFormat("dd").format(Date()).toInt()

        viewModel = ViewModelProviders.of(this).get(PlanViewModel::class.java)
        viewModel.getTodayPlan(current)

        rg.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { _, checkedId ->
            val index = when (checkedId) {
                R.id.unco -> 0
                R.id.co -> 1
                else -> 2
            }

            if (index == 0) {
                PlanRV.visibility = View.GONE
                UnPlanRV.visibility = View.VISIBLE
                viewModel.dataUnCompletePlan.observe(this,object :Observer<Plan>{
                    override fun onChanged(t: Plan?) {
                        uPlanadapter = PlanAdapter(
                            context!!,
                            R.layout.item_view,
                            viewModel.unCompletePlan
                        )
                        UnPlanRV.adapter = uPlanadapter
                        UnPlanRV.layoutManager = LinearLayoutManager(context)
                    }
                })
            } else if (index == 1) {
                PlanRV.visibility = View.VISIBLE
                UnPlanRV.visibility = View.GONE
                viewModel.dataCompletePlan.observe(this,object : Observer<Plan> {
                    override fun onChanged(t: Plan?) {
                        cPlanadapter = PlanAdapter(
                            requireContext(),
                            R.layout.item_view,
                            viewModel.completePlan
                        )
                        PlanRV.adapter = cPlanadapter
                        PlanRV.layoutManager = LinearLayoutManager(context)
                    }
                })
            }
        })


        btn_addStuff.setOnClickListener {
            findNavController().navigate(R.id.actionAddStuff)
        }
    }
}
