package com.hfut.android.plana.ui.plan

import androidx.lifecycle.ViewModel
import com.haibin.calendarview.Calendar
import com.hfut.android.plana.repository.Reposition

class AddStuffViewModel: ViewModel() {
    private val repository = Reposition()
    fun insertPlan(planName: String,
                   planType: String,
                   portraitId:Int,
                   startTime: Calendar,
                   endTime: Calendar,
                   frequency:Int,
                   taskNum:Int){
        repository.insertPlan(planName,
            planType,
            portraitId,
            startTime,
            endTime,
            frequency,
            taskNum)
    }
}