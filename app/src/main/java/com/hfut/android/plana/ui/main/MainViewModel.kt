package com.hfut.android.plana.ui.main

import androidx.lifecycle.ViewModel
import com.haibin.calendarview.Calendar
import com.hfut.android.plana.database.entity.Plan
import com.hfut.android.plana.repository.Reposition

class MainViewModel: ViewModel() {
    private val repository = Reposition()

    fun getTodayUnCompletePlan(date: Calendar): MutableList<Plan> {
        return repository.getTodayUncompletePlan(date)
    }

    fun completePlan(id: Int) {
        repository.completePlan(id)
    }

    fun refreshDailyPlan() {
        repository.refreshDailyPlan()
    }

    fun refreshWeeklyPlan() {
        repository.refreshWeeklyPlan()
    }
}