package com.hfut.android.plana


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.haibin.calendarview.Calendar
import com.hfut.android.plana.ui.main.MainViewModel
import com.hfut.android.plana.ui.main.adapter.ItemClickListener
import com.hfut.android.plana.ui.main.adapter.MainAdapter
import com.hfut.android.plana.ui.timetable.calendar.TimetableViewModel
import com.hfut.android.plana.ui.timetable.calendar.adapter.PlanAdapter
import kotlinx.android.synthetic.main.fragment_fragment_main.*
import kotlinx.android.synthetic.main.fragment_fragment_timetable.*
import java.text.SimpleDateFormat
import java.time.ZoneOffset.UTC
import java.util.*
import kotlin.concurrent.timerTask


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentMain : Fragment() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var mainAdapter: MainAdapter
    private lateinit var timer: Timer

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_main, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        super.onActivityCreated(savedInstanceState)
        cons_calendar.setOnClickListener {
            findNavController().navigate(R.id.actionTimetable)
        }
        cons_plan.setOnClickListener {
            findNavController().navigate(R.id.actionPlan)
        }
        mp_setting_icon.setOnClickListener {
            findNavController().navigate(R.id.actionSettings)
        }

        val todayOver = java.util.Calendar.getInstance()
        todayOver.setTimeInMillis(System.currentTimeMillis())
        todayOver.set(java.util.Calendar.HOUR_OF_DAY, 23)
        todayOver.set(java.util.Calendar.MINUTE, 11)
        todayOver.set(java.util.Calendar.SECOND, 30)
        todayOver.set(java.util.Calendar.MILLISECOND, 0)
        var todayOverDate = todayOver.time

        mp_refresh_icon.setOnClickListener {
            timer = Timer()
            timer.schedule(timerTask {
                Log.e("MyLog", "------每日刷新启动-------")
                run {
                    mainViewModel.refreshDailyPlan()
                }
            }, todayOverDate, 86400000)
        }

        val current = Calendar()
        current.year = SimpleDateFormat("yyyy").format(Date()).toInt()
        current.month = SimpleDateFormat("MM").format(Date()).toInt()
        current.day = SimpleDateFormat("dd").format(Date()).toInt()

        Log.e("MyLog", "-----------------------添加事项-----------${mainViewModel.getTodayUnCompletePlan(current)}---------------------")
        Log.e("MyLog", "-----------------------添加事项-----------${current}---------------------")

        mainAdapter = MainAdapter (
            context!!,
            R.layout.main_item_view,
            mainViewModel.getTodayUnCompletePlan(current),
            object: ItemClickListener {
                override fun onItemClick(position: Int) {
                    if (mainAdapter.data[position].isComplete == false) {
                        mainViewModel.completePlan(mainAdapter.data[position].id)
                    } else {

                    }
                }
            }
        )

        rv.adapter = mainAdapter
        rv.layoutManager = LinearLayoutManager(context)
    }
}
