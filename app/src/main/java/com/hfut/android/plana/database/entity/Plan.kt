package com.hfut.android.plana.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Author by yt and ob, Date on 7/5/2019.
 * PS: Consumed 3 bottles of fat-otaku-happy-water to write the code.
 */

@Entity(tableName = "PlanDatabase")
data class Plan(
    @ColumnInfo(name = "PLAN_NAME")
    var planName: String?,
    @ColumnInfo(name = "PLAN_TYPE")
    var planType: String?,
    @ColumnInfo(name = "PORTRAIT_ID")
    var portraitId: Int,
    @ColumnInfo(name = "START_TIME")
    var startTime: Long?,
    @ColumnInfo(name = "END_TIME")
    var endTime: Long?,
    @ColumnInfo(name = "FREQUENCY")
    var frequency: Int,
    @ColumnInfo(name = "COMPLETE_NUM")
    var completeNum: Int,
    @ColumnInfo(name = "INCOMPLETE_NUM")
    var incompleteNum: Int,
    @ColumnInfo(name = "IS_COMPLETE")
    var isComplete: Boolean?,
    @PrimaryKey(autoGenerate = true)
    var id: Int
)