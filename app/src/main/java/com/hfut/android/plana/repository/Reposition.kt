package com.hfut.android.plana.repository

import com.haibin.calendarview.Calendar
import com.hfut.android.plana.DAILY
import com.hfut.android.plana.WEEKLY
import com.hfut.android.plana.database.*
import com.hfut.android.plana.database.entity.Plan
import java.util.*

/**
 * Author by yt and ob, Date on 7/5/2019.
 * PS: Consumed 3 bottles of fat-otaku-happy-water to write the code.
 */
class Reposition{

    object Holder{
        val INSTANCE = Reposition()
    }

    companion object{
        val instance = Holder.INSTANCE
    }

    private val appDataBase = AppDataBase.instance

    //获取今天的计划
    fun getTodayPlan(date: Calendar):MutableList<Plan> {
        return appDataBase.getPlanDao().getDatePlan(date.timeInMillis)
    }

    //获取今天已完成的计划
    fun getTodayCompletePlan(date: Calendar):MutableList<Plan>{
        return appDataBase.getPlanDao().getTodayCompletePlan(date.timeInMillis)
    }

    //获取今天未完成的计划
    fun getTodayUncompletePlan(date: Calendar):MutableList<Plan>{
        return appDataBase.getPlanDao().getTodayUncompletePlan(date.timeInMillis)
    }


    //按给定date对象获取计划
    fun getPlanByDate(date: Date):MutableList<Plan> {
        return appDataBase.getPlanDao().getDatePlan(date.time)
    }

    //获取所有计划
    fun getAllPlans():MutableList<Plan> {
        return appDataBase.getPlanDao().getAllPlans()
    }

    //插入计划
    fun insertPlan (
        planName: String,
        planType: String,
        portraitId: Int,
        startTime: Calendar,
        endTime: Calendar,
        frequency: Int,
        taskNum: Int
        ) {
        val addedPlan = Plan(
            "defaultPlan",
            " ",
            0,
            0,
            1000,
            DAILY,
            0,
            1,
            false,
            0
        )
        addedPlan.planName = planName
        addedPlan.planType = planType
        addedPlan.portraitId = portraitId
        addedPlan.startTime = startTime.timeInMillis
        addedPlan.endTime = endTime.timeInMillis
        addedPlan.frequency = frequency
        addedPlan.incompleteNum = taskNum
        appDataBase.getPlanDao().insert(addedPlan)
    }

    //按名字删除计划
    fun deletePlanById(id:Int){
        appDataBase.getPlanDao().deletePlan(id)
    }

    //完成计划
    fun completePlan(id:Int){
        appDataBase.getPlanDao().completePlan(id)
    }

    //刷新每日计划
    fun refreshDailyPlan(){
        appDataBase.getPlanDao().refreshDailyPlan(DAILY)
    }

    //刷新每周计划
    fun refreshWeeklyPlan(){
        appDataBase.getPlanDao().refreshWeeklyPlan(WEEKLY)
    }

    //按id更新计划图片
    fun updatePlanIcon(id: Int,iconId:Int){
        appDataBase.getPlanDao().updateIconById(id,iconId)
    }
}
