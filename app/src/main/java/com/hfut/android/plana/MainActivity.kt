package com.hfut.android.plana

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import com.hfut.android.plana.database.entity.Plan
import com.hfut.android.plana.repository.Reposition
import java.util.*
import kotlin.concurrent.timerTask
import android.app.NotificationChannel
import android.graphics.Color


class MainActivity : AppCompatActivity() {

    private lateinit var timer:Timer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        timer = Timer()
        timer.schedule(timerTask {
            Log.i("log","log3")
            run {
                Log.i("log","log4")
                val date = Date()
                Log.i("log6",getDataByDate(date).size.toString())
                notifition(getDataByDate(date).size)
            }
        },3000,7000)
    }

    fun getDataByDate(date: Date): MutableList<Plan> {
        return Reposition.instance.getPlanByDate(date)
    }

    fun notifition(size:Int){
        Log.e("log","------------------${getSystemService(Context.NOTIFICATION_SERVICE)}")
        var mNm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O){
        val channel = NotificationChannel(
            "1",
            "Channel1", NotificationManager.IMPORTANCE_DEFAULT
        )
            channel.enableLights(true)
            channel.setLightColor(Color.RED)
            mNm.createNotificationChannel(channel)
        }
        Log.i("log9",size.toString())
        val intent = Intent()
        val pi = PendingIntent.getActivity(this,0,intent,0)
        val notifyBuild = NotificationCompat.Builder(this!!,"1")
            .setContentTitle("今天还有"+size+"个打卡!")
            .setContentText("要记得打卡哦！")
            .setAutoCancel(true)
            .setGroup("todos")
            .setGroupSummary(false)
            .setContentIntent(pi)
            .setLargeIcon(BitmapFactory.decodeResource(this?.resources,R.drawable.app_logo))
            .setSmallIcon(R.drawable.app_logo)
            .setShowWhen(true)
            .build().apply {
                visibility = Notification.VISIBILITY_PUBLIC
            }
        Log.i("log7",size.toString())
        mNm.notify(1,notifyBuild)

    }
}
