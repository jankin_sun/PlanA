package com.hfut.android.plana.ui.timetable.calendar

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.haibin.calendarview.Calendar
import com.hfut.android.plana.DAILY
import com.hfut.android.plana.database.AppDataBase
import com.hfut.android.plana.database.entity.Plan
import com.hfut.android.plana.repository.Reposition
import java.util.*


class TimetableViewModel: ViewModel() {
    private val repository = Reposition()

    val data = MutableLiveData<String>()
    fun getData(calendar: Calendar): MutableList<Plan> {
        return repository.getTodayPlan(calendar)
    }

    fun getDataByDate(date: Date): MutableList<Plan> {
        return repository.getPlanByDate(date)
    }

    fun getAllData(): MutableList<Plan> {
        return repository.getAllPlans()
    }

    fun insertPlan(planName: String,
                   planType: String,
                   potraitId: Int,
                   startTime: Calendar,
                   endTime: Calendar,
                   frequency: Int,
                   taskNum: Int
                   ) {
        repository.insertPlan(planName,
            planType,
            potraitId,
            startTime,
            endTime,
            frequency,
            taskNum
            )
    }
    //业务方法

    fun getUer(){
        //调用数据层方法
//        repository.getUserInfo()
    }
}