package com.hfut.android.plana.ui.plan.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hfut.android.plana.database.entity.Plan
import kotlinx.android.synthetic.main.item_view.view.*
import kotlinx.android.synthetic.main.main_item_view.view.*
import java.text.SimpleDateFormat
import java.util.*

class PlanAdapter(
    val context: Context,
    val itemLayout:Int,
    val data:MutableList<Plan>
): RecyclerView.Adapter<PlanAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(context).inflate(itemLayout,parent,false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val plan = data[position]
        holder.plan_name?.text = plan.planName
        holder.plan_img?.setImageResource(plan.portraitId)
        holder.plan_content.text = "开始时间： ${SimpleDateFormat("yyyy-MM-dd").format(Date(plan?.startTime!!))} \n" +
                "结束时间： ${SimpleDateFormat("yyyy-MM-dd").format(Date(plan?.endTime!!))}"
    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val plan_name = itemView.planName
        val plan_img = itemView.planIcon
        val plan_content = itemView.planContent
    }
}